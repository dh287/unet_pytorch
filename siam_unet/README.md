### Questions

1. Should `wandb` be even included?
2. Making training data for all the other features (i.e. LE and vertices)?
3. No need to merge the regular U-Net package anymore.
4. Weird prev_image augmentation bug
5. Weird tif bug
6. How generalizable is our method if an iteration needs to be cherry picked from Siam-UNet cosh?
7. self.imgs_shape[0] == 1 in predict.py